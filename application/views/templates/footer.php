<footer class="footer">
            <div class="footer-widgets">        
                <div class="container">
                    <div class="row"> 
                        <div class="col-md-3">
                            <div class="widget widget_text">
                                <h3 class="widget-title">About Us</h3>          
                                <div class="textwidget">
                                    <p>
                                        Ever wanted to know how to clean a squid or fillet a bream? Our chefs’ failsafe tips should prevent you from wasting a morsel.
                                    </p>
                                    <ul class="footer-info">
                                        <li class="address">Nelliparambu, Cherani, <br/>
                                            Manjeri-676121, Kerala.</li>
                                        <li class="email">Email us: info@kadalcfood.com</li>
                                        <li class="phone">Call us: +91-6282-727-097</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget widget_recent_posts">      
                                <h3 class="widget-title">Our complete menu</h3>       
                                <div class="textwidget">
                                    <p>
                                        On top of our extensive seafood selection, we have a menu of starters, sides, sushi and desserts, which is available here. For the full picture,  you’ll have to pop in at lunchtime and feast your eyes on the display. If you’re planning to impress a loved one or host a dinner party, you can pick up a couple of beauties to prepare at home.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget widget-link clearfix">
                                <h3 class="widget-title">User Links </h3>
                                <ul class="links">
                                    <li><a href="#.">About Us</a></li>
                                    <li><a href="#.">Blog</a></li>
                                    <li><a href="#.">Courses </a></li>
                                    <li><a href="#.">FAQs</a></li>
                                    <li><a href="#.">Events</a></li>
                                    <li><a href="#.">Support</a></li>
                                </ul>
                                <div class="social-links">
                                    <a href="#"><i class="fa fa-facebook-square"></i>Facebok</a>
                                    <a href="#"><i class="fa fa-twitter-square"></i>Twitter</a>
                                    <a href="#"><i class="fa fa-google-plus-square"></i>Google+</a>
                                    <a href="#"><i class="fa fa-linkedin-square"></i>Linkedin</a>
                                    <a href="#"><i class="fa fa-instagram"></i>Instagram</a>
                                    <a href="#"><i class="fa fa-pinterest-square"></i>Pinterest</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget widget_instagram">
                                <h3 class="widget-title">Our Moments</h3> 
                                <ul class="clearfix">                                
                                    <?php
				                    if (isset($galleries) and $galleries != false) {
				                        foreach ($galleries as $gallery){
				                        ?>
	                                    <li class="last">
	                                        <div class="thumb images-hover flat-hover-images">
	                                            <a href="#">
	                                                <span><img src="<?php echo $gallery->url . $gallery->file_name;?>" alt="<?php echo $gallery->name;?>"></span>
	                                            </a>
	                                        </div>
	                                    </li>
										<?php 
									 	}
									 }
									 ?>
                                </ul>  
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div class="footer-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <div class="copyright-content">
                                 Copyright © 2018. Designed by <a href=""><img src="img/cloudbery.png"></a>.</div>
                             </div>
                             <a class="go-top-v1"> <i class="fa fa-arrow-up"></i>  &nbsp;Back to Top</a>
                         </div>
                     </div>
                 </div>
             </div>
         </footer>
     </div>

     <!-- Javascript -->
     <script type="text/javascript" src="javascript/jquery.min.js"></script>
     <script type="text/javascript" src="javascript/jquery.datetimepicker.full.min.js"></script> 
     <script type="text/javascript" src="javascript/bootstrap.min.js"></script> 
     <script type="text/javascript" src="javascript/jquery.easing.js"></script> 
     <script type="text/javascript" src="javascript/imagesloaded.min.js"></script> 
     <script type="text/javascript" src="javascript/jquery.isotope.min.js"></script>
     <script type="text/javascript" src="javascript/jquery-waypoints.js"></script> 
     <script type="text/javascript" src="javascript/jquery.magnific-popup.min.js"></script>   
     <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
     <script type="text/javascript" src="javascript/parallax.js"></script>
     <script type="text/javascript" src="javascript/smoothscroll.js"></script>   
     <script type="text/javascript" src="javascript/jquery.flexslider-min.js"></script>
     <script type="text/javascript" src="javascript/owl.carousel.js"></script>
     <script type="text/javascript" src="javascript/jquery-validate.js"></script>
     <script type="text/javascript" src="javascript/jquery.fancybox.js"></script>
     <script type="text/javascript" src="javascript/main.js"></script>

     <!-- Revolution Slider -->
     <script type="text/javascript" src="javascript/jquery.themepunch.tools.min.js"></script>
     <script type="text/javascript" src="javascript/jquery.themepunch.revolution.min.js"></script>
     <script type="text/javascript" src="javascript/slider.js"></script>
</body>
</html>