<!DOCTYPE html> 
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>KADAL | The Seafood Restaurant</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon" sizes="48x48">
    <link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="icon/favicon.png" rel="shortcut icon">
    <!-- =========== Style Sheets =========== -->

    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">
    <link rel="stylesheet" type="text/css" href="stylesheets/animate.css">
</head>    
<body class=""> 
    <section class="loading-overlay">
        <div class="Loading-Page">
            <h2 class="loader">Loading</h2>
        </div>
    </section>
    <div class="box">
        <header id="header" class="header clearfix">
            <div class="header-wrap clearfix">
                <div class="container">
                    <div class="logo-mobi"><a href="#"> <img src="img/logo.png" alt="image"></a></div>
                <div class="btn-menu">
                <span></span>
                </div>
                    <nav id="mainnav" class="mainnav">
                        <ul class="menu">
                            <li> <a href="index">Home </a></li>
                            <li> <a href="about">About Us</a></li>
                            <li> <a href="menu">Our Menu</a></li>
                            <li class="logo">
                                <a href="index"> <img src="img/logo.png" alt="image"></a>
                            </li>
                            <li> <a href="testimonial">Clients Says</a></li>
                            <li> <a href="gallery">Gallery</a></li>
                            <li> <a href="contact">Contact</a></li>
                        </ul>                       
                    </nav>
                </div>
            </div>
        </header>  