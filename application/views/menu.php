 
        <section class="flat-row menu-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-section style1 ">
                            <div class="top-section">
                                <p>Discover</p>
                            </div>
                            <h1 class="title">Menu Hot</h1>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <?php
                    if (isset($menus) and $menus) {
                        foreach ($menus as $menu) {
                        ?>
                        <div class="col-sm-3 col-xs-6">
                            <div class="product effect1">
                                <div class="box-wrap">
                                    <div class="box-image">
                                        <a href="#"><img src="<?php echo $menu->url . $menu->file_name;?>" alt="<?php echo $menu->name;?>"></a>
                                    </div>
                                    <div class="box-content">
                                       <h6><?php echo $menu->name;?></h6>
                                       <ul>
                                            <li>&#8377;<?php echo $menu->price;?></li>
                                            <li>
                                                <i class="fa fa-heart"></i>
                                                <i class="fa fa-heart"></i>
                                                <i class="fa fa-heart"></i>
                                                <i class="fa fa-heart"></i>
                                                <i class="fa fa-heart"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                    }
                   ?>
                   
                </div>
            </div>
        </section>
        

       