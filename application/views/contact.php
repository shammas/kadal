
        <section class="flat-row contact-1 ">
            <div class="container">
                <div class="row">
                    <div class="flat-information col-sm-4">
                        <span class="fa fa-map-marker" aria-hidden="true"></span>
                        <h5 class="information-title">ADRESS</h5>
                        <p class="address">Nelliparambu, Cherani, <br/> Manjeri-676121, Kerala.</p>
                    </div>
                    <div class="flat-information email col-sm-4">
                        <span class="fa fa-envelope" aria-hidden="true"></span>
                        <h5 class="information-title">EMAIL</h5>
                        <a href="mailto:info@kadalcfood.com">info@kadalcfood.com</a>
                    </div>
                    <div class="flat-information phone col-sm-4">
                        <span class="fa fa-phone" aria-hidden="true"></span>
                        <h5 class="information-title">HOTLINE</h5>
                        <a href="tel:+91-6282-727-097">+91-6282-727-097</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="flat-row contact-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="reservation-page-right">
                            <img src="images/maketrevation/1.png" class="img-responsive" alt="reservation-banner"> 
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="reservation-page-left">
                            <div class="reservation-page-form"> 
                                <div class="title-section">
                                    <h1 class="title">Contact Us</h1>
                                </div>
                                <form id="reservation-form" action="contact/contact-process.php" novalidate="novalidate">                                        
                                    <div class="reservation-page-input-box">
                                        <label>Your name</label>
                                        <input type="text" class="form-control" placeholder="Full name" name="name" id="form-name" data-error="Subject field is required" required="">
                                    </div>
                                    <div class="reservation-page-input-box">
                                        <label>Your email</label>
                                        <input type="email" class="form-control" placeholder="Enter your email" name="email" id="form-email" data-error="Subject field is required" required="">
                                    </div>
                                    <div class="reservation-page-input-box">
                                        <label>Phone number</label>
                                        <input type="text" class="form-control" placeholder="Your phone" name="phone" id="form-phone" data-error="Subject field is required" required="">
                                    </div>
                                    <div class="reservation-page-input-box">
                                        <label>Subjects</label>
                                        <input type="text" class="form-control rt-date" placeholder="Subjects" name="subjects" id="form-subjects" data-error="Subject field is required" required="">
                                    </div>

                                    <div class="reservation-page-textarea">
                                        <label>Message</label>
                                        <textarea class="form-control your-message" placeholder="Your message" name="message" id="form-message"></textarea>
                                    </div>

                                    <div class="reservation-booking">
                                        <button type="submit" class="book-now-btn">SUBMIT</button>
                                    </div>
                                </form>                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>        
       