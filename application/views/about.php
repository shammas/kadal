 
        
        <section class="pt10">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="img/home-side-1.png" alt="images">
                    </div>
                    <div class="col-md-6">
                        <div class="flat-divider d96px"></div>
                        <div class="wrap-content-story">
                            <div class="title-section style2 ">
                                <h1 class="title">Like it fresh?</h1>
                            </div>
                            <p class="content-story">
                                Our local fish and shellfish is hauled directly from dhow to counter-top on a daily basis, but we also go to great lengths to import the finest foreign varieties: our salmon, mussels, clams, scallops, sea bass, tuna, mud crabs, cockles and oysters arrive twice a week from Europe, and we make sure they travel from source to plate with as little fuss as possible. All of these offerings are all lined up in the market area, all shimmery-gilled and bright-eyed, so you can pick your dinner. The daily list is made up of 47 items, among which Emirati favourites include sherri fish, hammour and safi.
                            </p>
                            <p class="content-story">
                                We always think of you. Aside from our a la carte menu and fresh seafood market varieties, our Chefs created something special from the region to capture your senses. Please check with our team for today’s Daily Chef Special.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="flat-row flat-our">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="flat-divider d96px"></div>
                        <div class="wrap-content-story">
                            <div class="title-section style2 ">
                                <h1 class="title">Fresh Fish from the sea to your table…</h1>
                            </div>
                            <p class="content-story">
                                Giving you the full ocean-to-table experience, right by the beach in contemporary Katara, this is where you want to dine in Doha. Once you’ve spotted the most alluring fish or crustacean in our counter (charged by weight), you can decide how you’d like it cooked – in Mediterranean, Far Eastern, Arabic or Japanese style. Our waiters can then guide you through the selection of starters, salads, soups and drinks. The dining room is buzzing and vast, so you might be seated on fuchsia floor cushions, on cream leather banquettes, or on stools up at the bar - and while some tables have sea views, others invite you to keep a watchful eye on the slick open kitchen.
                            </p>
                            <p class="content-story">
                                KADAL restaurant is now serving fresh fish that are fished-out by the Qatari Fishermen. Fresh fish are available for dine in or take-away. Please call 7706 0230 – Bu Mahammad to know more. *Terms and conditions apply.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <img src="img/home-side-2.png" alt="images">
                    </div>
                </div>
            </div>
        </section>
        <section class="flat-row flat-testimonial index-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-section margb-27px">
                            <h1 class="title2">Our Customers Say</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-carousel">
                            <div class="owl-carousel-services">
                                <div class="item-owl">
                                    <div class="blockquote-testimo">
                                        <p> "A great Template for my website, my first reason is the design but I would have put <br/> all of them. One of the best templates I found, simple, easy, very good customer support, 
                                            very easy to install...a very good template. Thank NthPsd

                                        </p>                        
                                    </div>

                                    <div class="title-testimonial">
                                        <h6 class="title"><a href="#">Eugene Fernandez</a></h6>
                                        <p>Cloudbery Solutions</p>
                                    </div>
                                </div>

                                <div class="item-owl">
                                    <div class="blockquote-testimo">
                                        <p> "A great Template for my website, my first reason is the design but I would have put <br/> all of them. One of the best templates I found, simple, easy, very good customer support, very easy to install...a very good template. Thank NthPsd"
                                        </p>                        
                                    </div>

                                    <div class="title-testimonial">
                                        <h6 class="title-post"><a href="#">Eugene Fernandez</a></h6>
                                        <p>Cloudbery Solutions</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        