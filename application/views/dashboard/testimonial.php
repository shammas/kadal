<div class="wrapper wrapper-content animated fadeInRight"  >
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : !files, 'col-lg-9' : files}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add testimonial</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" ng-submit="addTestimonial()" >
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Name</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.name}">
                                <input type="text" placeholder="Your Name here" class="form-control" ng-model="newtestimonial.name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Designation</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.designation}">
                                <input type="text" placeholder="Designation or firm" class="form-control" ng-model="newtestimonial.designation">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Description</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.description}">
                                <textarea name="description" class="form-control" placeholder="Type here" ng-model="newtestimonial.description"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit"  ng-bind="(curtestimonial == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All testimonials</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="">
                        <button type="button" class="btn btn-primary" ng-click="newTestimonial()">
                            Add a new testimonial
                        </button>
                    </div>
                    <div class="table-responsive">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="dataTables_length" id="DataTables_Table_0_length">
                                <label>
                                    <select  aria-controls="DataTables_Table_0" class="form-control input-sm" ng-model="numPerPage"
                                             ng-options="num for num in paginations">{{num}}
                                    </select>
                                    entries
                                </label>
                            </div>
                            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                <label>Search:<input class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0" type="search"></label>
                            </div>
                            <table class="table table-striped table-bordered table-hover dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
                                <thead>
                                <tr role="row">
                                    <th>Sl No</th>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="gradeA odd" role="row" dir-paginate="testimonial in testimonials | filter:search | limitTo:pageSize | itemsPerPage:numPerPage" current-page="currentPage">

                                    <td class="sorting_1">{{$index+1}}</td>
                                    <td>{{testimonial.name}}</td>
                                    <td>{{testimonial.designation}}</td>
                                    <td>{{testimonial.description}}</td>
                                    <td class="center">
                                        <div  class="btn-group btn-group-xs" role="group">
                                            <button type="button" class="btn btn-info" ng-click="editTestimonial(testimonial)">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button  type="button" class="btn btn-danger" ng-click="deleteTestimonial(testimonial)">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-4">

                                <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                    Showing {{currentPage}} to {{(numPerPage < testimonials.length  ? currentPage*numPerPage :testimonials.length)}} of {{testimonials.length}} entries
                                </div>
                                <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                    Showing {{(currentPage-1)*numPerPage+1}} to {{(currentPage*numPerPage)}} of {{testimonials.length}} entries
                                </div>
                            </div>



                            <div class="col-md-8 pull-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true">
                                    </dir-pagination-controls>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                       
                       