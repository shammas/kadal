
        <section class="flat-row flat-testimonial index-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-section margb-27px">
                            <h1 class="title2">Our Customers Say</h1>
                            <p class="title-center">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                <?php
                if (isset($testimonials) and $testimonials) {
                    foreach ($testimonials as $testimonial) {
                    ?>
                    <div class="col-md-4">
                        <div class="testimonial-grid">
                            <p class="desc">
                                "<?php echo $testimonial->description;?>"
                            </p>
                            <div class="test-author">
                                <h6><?php echo $testimonial->name;?></h6>
                                <p><?php echo $testimonial->designation;?></p>
                            </div>
                        </div>
                    </div>
                    <?php 
                    }
                }
                ?>
                </div>
            </div>
        </section>
        
        
       