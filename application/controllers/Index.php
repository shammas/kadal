<?php
/**
 * Index.php
 * Date: 06/03/19
 * Time: 03:03 PM
 */

class Index extends CI_Controller { 
	 
        protected $header = 'templates/header';
        protected $footer = 'templates/footer';

        public function __construct()
        {
            parent::__construct();

            $this->load->model('Testimonial_model', 'testimonial');
            $this->load->model('Menu_model', 'menu');
            $this->load->model('Gallery_model', 'gallery');
        }

        protected $current = '';

        public function index()
        {
            $data['testimonials'] = $this->testimonial->order_by('id','desc')->get_all();
            $data1['galleries'] = $this->gallery->order_by('id','desc')->limit(6)->get_all();
           
            $this->current = 'index';
            $this->load->view($this->header, ['current' => $this->current]);
            $this->load->view('index',$data);
            $this->load->view($this->footer,$data1);
        }

        public function about()
        {
            $data1['galleries'] = $this->gallery->order_by('id','desc')->limit(6)->get_all();

            $this->current = 'about';
            $this->load->view($this->header, ['current' => $this->current]);
            
            $this->load->view('about');
            $this->load->view($this->footer,$data1);
        }

        public function contact() 
        {
            $data1['galleries'] = $this->gallery->order_by('id','desc')->limit(6)->get_all();

            $this->current = 'contact';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('contact');
            $this->load->view($this->footer,$data1);
        }

        

        public function gallery()
        {
            $data1['galleries'] = $this->gallery->order_by('id','desc')->limit(6)->get_all();

            $this->current = 'gallery';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('gallery');
            $this->load->view($this->footer,$data1);
        }

        public function menu()
        {
            $data['menus'] = $this->menu->order_by('id','desc')->get_all();
            $data1['galleries'] = $this->gallery->order_by('id','desc')->limit(6)->get_all();

            $this->current = 'menu';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('menu',$data);
            $this->load->view($this->footer,$data1);
        }

        public function testimonial()
        {
            $data['testimonials'] = $this->testimonial->order_by('id','desc')->get_all();
            $data1['galleries'] = $this->gallery->order_by('id','desc')->limit(6)->get_all();

            $this->current = 'testimonial';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('testimonial',$data);
            $this->load->view($this->footer,$data1);
        }
}