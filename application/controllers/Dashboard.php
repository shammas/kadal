<?php
/**
 * Dashboard.php
 * Date: 05/03/19
 * Time: 11:41 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library(['encryption', 'ion_auth']);
        /*
         * Check login
         * */
        if (!$this->ion_auth->logged_in())
        {
            // Allow some methods?
            $allowed = array(
                'test'
            );
            if ( ! in_array($this->router->fetch_method(), $allowed))
            {
                // redirect them to the login page
                redirect(base_url('login'), 'refresh');
            }
        }
    }

    public function test($param="")
    {
        var_dump(CI_VERSION);
        $date = strtotime('2018-01-09 11:31:22');
        var_dump(date("M"), $date);

    }

    //////////////////////////////////////////////

    public function index()
    {
        $this->load->view('dashboard/index');
    }

    public function page($page)
    {
        if (file_exists(realpath(APPPATH . 'views/dashboard/' . $page . '.php'))) {
            $this->load->view('dashboard/' . $page);
        } else {
            show_404('errors/html/error_404');
        }
    }

    public function thumbnail_check()
    {
        $resize_error = [];
        $uploads = $this->_list_files(getwdir() . 'uploads');
        $upload_count = sizeof($uploads);
        $thumbs = $this->_list_files(getwdir() . 'uploads/thumb');
        $thumbs_count = sizeof($thumbs);
        if ($thumbs_count > $upload_count) {
            $temp = $thumbs;
            foreach ($temp as $key => $value) {
                $temp[$key] = str_replace('thumb_', '', $value);
            }
            $diff = array_diff($temp, $uploads);
            if (!empty($diff)) {
                foreach ($diff as $value) {
                    if (file_exists(getwdir() . '/uploads/thumb/thumb_' . $value)) {
                        unlink(getwdir() . 'uploads/thumb/thumb_' . $value);
                    }
                }
            }
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'thumbnail refresh complete']));
        } elseif ($thumbs_count < $upload_count) {

            $temp = $thumbs;
            foreach ($temp as $key => $value) {
                $temp[$key] = str_replace('thumb_', '', $value);
            }
            $diff = array_diff($uploads, $temp);
            if (!empty($diff)) {
                foreach ($diff as $image) {
                    $img_cfg['image_library'] = 'gd2';
                    $img_cfg['maintain_ratio'] = TRUE;
                    $img_cfg['height'] = 300;
                    $img_cfg['width'] = 300;
                    $img_cfg['quality'] = 80;
                    $img_cfg['master_dim'] = 'height';
                    $img_cfg['source_image'] = realpath(getwdir() . 'uploads/' . $image);
                    $img_cfg['new_image'] = getwdir() . 'uploads/thumb/thumb_' . $image;
                    $this->image_lib->initialize($img_cfg);
//                    list($width, $height, $type) = getimagesize(getwdir() . '/uploads/' . $image);
                    if (!$this->image_lib->resize()) {
                        array_push($resize_error, $this->image_lib->display_errors());
                    }
                    $this->image_lib->clear();
                }
            }
            if (empty($resize_error)) {
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'thumbnail refresh complete']));
            }else{
                $this->output->set_status_header(500, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'please refresh page']));
            }

        } else {
            $temp = $thumbs;
            foreach ($temp as $key => $value) {
                $temp[$key] = str_replace('thumb_', '', $value);
            }
            if ($diff_temp = array_diff($temp, $uploads)) {
                foreach ($diff_temp as $image) {
                    if (file_exists(getwdir() . '/uploads/thumb/thumb_' . $image)) {
                        unlink(getwdir() . '/uploads/thumb/thumb_' . $image);
                    }
                }
                $this->thumbnail_check();
            }
        }
    }


    /**
     * @param directory
     * @return array files
     */
    function _list_files($dir)
    {
        $files = [];
        if ($handle = opendir($dir)) {
            $count = 0;
            while (false !== ($images = readdir($handle))) {

                if ($images != "." && $images != "..") {
                    if (is_file($dir."/".$images)) {
                        $count++;
                        array_push($files, $images);
                    }
                }
            }
            closedir($handle);
            return $files;
        }
    }

    public function load_user()
    {
        $this->output->set_content_type('application/json')->set_output(json_encode($this->ion_auth->user()->row()));
    }



}
