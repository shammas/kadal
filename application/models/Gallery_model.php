<?php
/**
 * Gallery_model.php
 * Date: 06/03/19
 * Time: 03:31 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Gallery_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}