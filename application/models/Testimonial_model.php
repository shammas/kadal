<?php
/**
 * Testimonial_model.php
 * Date: 06/03/19
 * Time: 03:47 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Testimonial_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}